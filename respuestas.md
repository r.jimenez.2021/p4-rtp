## Ejercicio 3. Análisis general
* ¿Cuántos paquetes componen la captura?: La captura esta compuesta de 1268 paquetes.
* ¿Cuánto tiempo dura la captura?: La captura dura 12.810068000 segundos
* Aproximadamente, ¿cuántos paquetes se envían por segundo?: Se envían 98.984642392 paquetes por segundo
* ¿Qué protocolos de nivel de red aparecen en la captura?: IPv4.
* ¿Qué protocolos de nivel de transporte aparecen en la captura?: UDP.
* ¿Qué protocolos de nivel de aplicación aparecen en la captura?: RTP.
## Ejercicio 4. Análisis de un paquete
* Número aleatorio elegido (N): 22
* Dirección IP de la máquina A: 216.234.64.16
* Dirección IP de la máquina B: 192.168.0.10
* Puerto UDP desde el que se envía el paquete: 54550
* Puerto UDP hacia el que se envía el paquete: 49154
* SSRC del paquete: 0x31be1e0e
* Tipo de datos en el paquete RTP (según indica el campo correspondiente): ITU-T G.711 PCMU (0)
## Ejercicio 5. Análisis de un flujo (stream)
* ¿Cuál es el primer número de tu DNI / NIE?: 7
* ¿Cuántos paquetes hay en el flujo F?: 642
* ¿El origen del flujo F es la máquina A o B?: El origen es la maquina B
* ¿Cuál es el puerto UDP de destino del flujo F?: 54550
* ¿Cuántos segundos dura el flujo F?: 12.81
## Ejercicio 6. Métricas del flujo
* ¿Cuál es la diferencia máxima en el flujo?: 31.653000 ms
* ¿Cuál es el jitter medio del flujo?: 12.234262 ms
* ¿Cuántos paquetes del flujo se han perdido?: 0 paquetes
* ¿Cuánto tiempo (aproximadamente) está el jitter por encima de 0.4 ms?: Esta siempre por encima.
## Ejercicio 7. Métricas de un paquete RTP
* ¿Cuál es su número de secuencia dentro del flujo RTP?: 18446
* ¿Ha llegado pronto o tarde, con respecto a cuando habría debido llegar?: Ha llegado tarde
* ¿Qué jitter se ha calculado para ese paquete? 0.589354
* ¿Qué timestamp tiene ese paquete? 1769307243
* ¿Por qué número hexadecimal empieza sus datos de audio? b
## Ejercicio 8. Captura de una traza RTP
* Salida del comando `date`: lun 23 oct 2023 09:12:40 CEST
* Número total de paquetes en la captura: 1201 paquetes
* Duración total de la captura (en segundos): 9.265394557 segundos
## Ejercicio 9. Análisis de la captura realizada
* ¿Cuántos paquetes RTP hay en el flujo?:  450 paquetes
* ¿Cuál es el SSRC del flujo?:  0x529c3095
* ¿En qué momento (en ms) de la traza comienza el flujo? 6266.051109 ms
* ¿Qué número de secuencia tiene el primer paquete del flujo? 15621
* ¿Cuál es el jitter medio del flujo?: 0 
* ¿Cuántos paquetes del flujo se han perdido?: 0
## Ejercicio 10 (segundo periodo). Analiza la captura de un compañero
* ¿De quién es la captura que estás usando?: Joan Andreu Aristizabal Guillen
* ¿Cuántos paquetes de más o de menos tiene que la tuya? (toda la captura):9264 - 1201 = 8063 paquete tiene de mas respecto la mia
* ¿Cuántos paquetes de más o de menos tiene el flujo RTP que el tuyo?: Tenemos los mismos
* ¿Qué diferencia hay entre el número de secuencia del primer paquete de su flujo RTP con respecto al tuyo?:Mi primer paquete tiene numero de secuencia 15621 y el suyo 28656
* En general, ¿que diferencias encuentras entre su flujo RTP y el tuyo?: Empieza mas tarde, los valores del tiempo de flujo, las deltas, el source port, el SSRC y pequeñas variaciones en el jitter
